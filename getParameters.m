function [N,parameters] = getParameters(ID)

    ID9 = mod(ID,10);
    ID8 = mod(floor(ID/10),10);
    ID7 = mod(floor(ID/100),10);

    N = mod(ID9,5)+1;
    if(N == 4)
        a = 2 + mod(ID8,5)*0.5;
    else
        a = 15 + mod(ID8,5)*15;
    end

    b = 6 + mod(ID7*ID8,5)*0.5;
    c = 2 + mod(ID7*ID8,5)*0.5;
    e = 2 + mod(ID9+ID8,5)*0.5;
    d = (8 - e)+mod(ID9+ID8,5)*0.5;
    d = d + (10-(e+d));
    f = ID9;

    parameters = [a b c d e f];
end