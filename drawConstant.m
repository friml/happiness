function drawConstant(K, ID9, mine, maxe, minw, maxw, color)
    %% arrows
    [X,Y] = meshgrid(mine+((maxe-mine)/2),linspace(minw,maxw,10));
    
    U = Y;
    V = -K-ID9*Y;
    UN=U./sqrt(U.^2+V.^2);
    VN=V./sqrt(U.^2+V.^2);
 
    quiver(X,Y,UN,VN,0.2, 'color',color)
    %% lines
    [X,Y] = meshgrid([mine maxe],linspace(minw,maxw,10));
    plot(X',Y', 'color',color)
    %% colorshape
    pgon = polyshape([min(min(X)) min(min(X)) max(max(X)) max(max(X))],...
        [max(max(Y)) min(min(Y)) min(min(Y)) max(max(Y))]);
    plot(pgon, 'FaceAlpha',0.1, 'LineStyle', 'None', 'FaceColor', color)      
end