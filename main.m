% This is source of cries and tears. 
% If you don't know what it is, you should not read this.
% If you know what it is, you should probably not read this either. 
% So close this file and go watch questionable videos on the internet.

function [f] = main(ID)

    if or(ID>999999, ID<100000)
        disp("Wrong ID")
        return
    end
    
    w = [-8,10];
    close all
    
    [N,params] = getParameters(ID);
    
    a = params(1);
    b = params(2);
    c = params(3);
    d = params(4);
    e = params(5);
    f = mod(params(6),3)+1;

    nonlinearityDescriptor(ID)        
        
    cols = colororder;
    lineSpec = '-k';    
    
    switch N
    case 1
        figure(1); hold on; grid on; grid minor
        drawConstant(0, f, -e, d, w(1), w(2), cols(1,:))
        drawConstant(b-c, f, d, d+2, w(1), w(2), cols(2,:))
        drawLinear(tand(a), -tand(a)*(-e)-c, f, -(e+2), -(e+2)+(c+2*tand(a))/tand(a), w(1), w(2), 'left', cols(3,:))
        
        figure(1); hold on; grid on; grid minor
        quiver(d,0,-e-d,0,1,lineSpec);
        quiver(-e,0,0,-c,1,lineSpec);
        quiverNoArrow(d,0,0,b-c,1,lineSpec);
        quiverNoArrow(d,b-c,2,0,1,lineSpec);
        quiver(-(e+2),-(c+2*tand(a)),(c+2*tand(a))/tand(a),c+2*tand(a),1,lineSpec);   
		
    case 2
        figure(1); hold on; grid on; grid minor
        drawConstant(-c, f, -(d-e)-2+2,-(d-e)-2, w(1), w(2), cols(1,:))
        drawConstant(0, f, -(d-e),e+e, w(1), w(2), cols(2,:))
        drawLinear(tand(a), -tand(a)*(2*e), f, 2*e, 2*e+(b-c)/tand(a), w(1), w(2), 'right', cols(3,:))
        drawConstant((b-c), f, 2*e+(b-c)/tand(a)-e,2+2*e+(b-c)/tand(a), w(1), w(2), cols(4,:))
        drawLinear(tand(a), -tand(a)*(e), f, (b-c)/tand(a)+e+(c-b)/tand(a), (b-c)/tand(a)+e, w(1), w(2), 'right', cols(5,:))

        
        figure(1); hold on; grid on; grid minor
        quiverNoArrow(-(d-e)-2,-c,2,0,1,lineSpec);
        quiverNoArrow(-(d-e),-c,0,c,1,lineSpec);
        quiverNoArrow(-(d-e),0,d,0,1,lineSpec);
        quiver(e,0,e,0,1,lineSpec);
        quiver(2*e,0,(b-c)/tand(a),b-c,1,lineSpec);
        quiverNoArrow(2+2*e+(b-c)/tand(a),(b-c),-2,0,1,lineSpec);
        quiver(2*e+(b-c)/tand(a),(b-c),-e,0,1,lineSpec);
        quiver((b-c)/tand(a)+e,(b-c),(c-b)/tand(a),(c-b),1,lineSpec);
        
    case 3
        figure(1); hold on; grid on; grid minor
        drawConstant(c-b, f, -e-2, -e-2+2+d, w(1), w(2), cols(1,:))
        drawLinear(tand(a), (c-b)-tand(a)*((d-e)), f, d-e, d-e+b/tand(a), w(1), w(2), 'right', cols(2,:))
        drawConstant(c, f, 0, d-e+b/tand(a), w(1), w(2), cols(3,:))
        drawConstant(0, f, -e, 0, w(1), w(2), cols(4,:))
        
        figure(1); hold on; grid on; grid minor
        quiver(-e-2,c-b, 2+d, 0, 1, lineSpec);
        quiver(d-e,c-b,b/tand(a),b,1,lineSpec);
        quiverNoArrow(d-e+b/tand(a)+2, c,-2,0,1,lineSpec);
        quiver(d-e+b/tand(a),c,e-d-b/tand(a),0,1,lineSpec);
        quiver(0,c,0,-c,1,lineSpec);
        quiver(0,0,-e,0,1,lineSpec);
        quiver(-e,0,0,(c-b),1,lineSpec);
        
    case 4
        figure(1); hold on; grid on; grid minor
        drawConstant(-c, f, -2-e, -2-e+2+(e-a/2), w(1), w(2), cols(1,:))
        drawConstant(0, f, -e, -a/2+a/2+d, w(1), w(2), cols(2,:))
        drawConstant(b-c, f, d+2-(2+d-a/2), d+2, w(1), w(2), cols(3,:))
        
        figure(1); hold on; grid on; grid minor
        quiver(-2-e,-c,2+(e-a/2),0,1,lineSpec);
        quiver(-a/2,-c,0,c,1,lineSpec);
        quiver(-a/2,0,a/2+d,0,1,lineSpec);
        quiver(d,0,0,b-c,1,lineSpec);
        quiver(d+2,b-c,-(2+d-a/2),0,1,lineSpec);
        quiver(a/2,b-c,0,c-b,1,lineSpec);
        quiver(0,0,-e,0,1,lineSpec);
        quiver(-e,0,0,-c,1,lineSpec);
        
    case 5
        figure(1); hold on; grid on; grid minor
        k=(b-c)/2;
        drawLinear(k, -k*(-d+e), f, -(d-e)-2, -(d-e), w(1), w(2), 'left', cols(1,:))
        drawConstant(0, f, -(d-e), -(d-e)+d, w(1), w(2), cols(2,:))
        drawLinear(tand(a), -tand(a)*(e), f, e, e+c/tand(a)+2, w(1), w(2), 'right', cols(3,:))
        drawConstant(c, f, e+c/tand(a)-e, e+c/tand(a), w(1), w(2), cols(4,:))
        drawLinear(tand(a), 0, f, 0, c/tand(a), w(1), w(2), 'right', cols(5,:))
        
        figure(1); hold on; grid on; grid minor
        quiverNoArrow(-(d-e)-2,c-b,2,b-c,1,lineSpec);
        quiver(-(d-e),0,d,0,1,lineSpec);
        quiver(e,0,c/tand(a),c,1,lineSpec);
        quiverNoArrow(e+c/tand(a),c,2,2*tand(a),1,lineSpec);
        quiver(e+c/tand(a),c,-e,0,1,lineSpec);
        quiver(c/tand(a),c,-c/tand(a),-c,1,lineSpec);
        
    otherwise
        disp("Wrong ID")
        
    end
    
    ylim(w)
end






