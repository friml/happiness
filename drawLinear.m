function drawLinear(K, q, ID9, mine, maxe, minw, maxw, lr, color)
    %% arrows
    if strcmp(lr, 'left')
        [X,Y] = meshgrid(mine,linspace(minw,maxw,10));
    else
        [X,Y] = meshgrid(maxe,linspace(minw,maxw,10));
    end
    
    U = Y;
    V = -K.*X-q-ID9*Y;
    UN=U./sqrt(U.^2+V.^2);
    VN=V./sqrt(U.^2+V.^2);
    UN(isnan(UN)) = 0;
    VN(isnan(VN)) = 0;

    quiver(X,Y,UN,VN,0.2,'color', color)
    %% lines
    [X,Y] = meshgrid([mine maxe],linspace(minw,maxw,10));
    w = Y(:,1);
    if strcmp(lr, 'left')
        kappas = -K./w*mine-q./w-ID9;
    else
        kappas = -K./w*maxe-q./w-ID9;
    end    
    Y = -K./(kappas+ID9).*X-q./(kappas+ID9);
    Y(isnan(Y)) = 0;
    plot(X',Y', 'color',color)
    %% colorshape
    pgon = polyshape([min(min(X)) min(min(X)) max(max(X)) max(max(X))],...
        [max(max(Y)) min(min(Y)) min(min(Y)) max(max(Y))]);
    plot(pgon, 'FaceAlpha',0.1, 'LineStyle', 'None', 'FaceColor', color)      
end