% This is source of cries and tears. 
% If you don't know what it is, you should not read this.
% If you know what it is, you should probably not read this either. 
% So close this file and go watch questionable videos on the internet.

function [f] = nonlinearityGenerator(ID)

    if or(ID>999999, ID<100000)
        disp("Wrong ID")
        return
    end
    
    w = [-8,10];
    close all
    
    [N,params] = getParameters(ID);
    
    a = params(1);
    b = params(2);
    c = params(3);
    d = params(4);
    e = params(5);
    f = mod(params(6),3)+1;
     
        
    lineSpec = '-k';    
    
    switch N
    case 1        
        figure(1); hold on; grid on; grid minor
        quiver(d,0,-e-d,0,1,lineSpec);
        quiver(-e,0,0,-c,1,lineSpec);
        quiverNoArrow(d,0,0,b-c,1,lineSpec);
        quiverNoArrow(d,b-c,2,0,1,lineSpec);
        quiver(-(e+2),-(c+2*tand(a)),(c+2*tand(a))/tand(a),c+2*tand(a),1,lineSpec);   
		
    case 2      
        figure(1); hold on; grid on; grid minor
        quiverNoArrow(-(d-e)-2,-c,2,0,1,lineSpec);
        quiverNoArrow(-(d-e),-c,0,c,1,lineSpec);
        quiverNoArrow(-(d-e),0,d,0,1,lineSpec);
        quiver(e,0,e,0,1,lineSpec);
        quiver(2*e,0,(b-c)/tand(a),b-c,1,lineSpec);
        quiverNoArrow(2+2*e+(b-c)/tand(a),(b-c),-2,0,1,lineSpec);
        quiver(2*e+(b-c)/tand(a),(b-c),-e,0,1,lineSpec);
        quiver((b-c)/tand(a)+e,(b-c),(c-b)/tand(a),(c-b),1,lineSpec);
        
    case 3
        figure(1); hold on; grid on; grid minor
        quiver(-e-2,c-b, 2+d, 0, 1, lineSpec);
        quiver(d-e,c-b,b/tand(a),b,1,lineSpec);
        quiverNoArrow(d-e+b/tand(a)+2, c,-2,0,1,lineSpec);
        quiver(d-e+b/tand(a),c,e-d-b/tand(a),0,1,lineSpec);
        quiver(0,c,0,-c,1,lineSpec);
        quiver(0,0,-e,0,1,lineSpec);
        quiver(-e,0,0,(c-b),1,lineSpec);
        
    case 4
        figure(1); hold on; grid on; grid minor
        quiver(-2-e,-c,2+(e-a/2),0,1,lineSpec);
        quiver(-a/2,-c,0,c,1,lineSpec);
        quiver(-a/2,0,a/2+d,0,1,lineSpec);
        quiver(d,0,0,b-c,1,lineSpec);
        quiver(d+2,b-c,-(2+d-a/2),0,1,lineSpec);
        quiver(a/2,b-c,0,c-b,1,lineSpec);
        quiver(0,0,-e,0,1,lineSpec);
        quiver(-e,0,0,-c,1,lineSpec);
        
    case 5
        figure(1); hold on; grid on; grid minor
        quiverNoArrow(-(d-e)-2,c-b,2,b-c,1,lineSpec);
        quiver(-(d-e),0,d,0,1,lineSpec);
        quiver(e,0,c/tand(a),c,1,lineSpec);
        quiverNoArrow(e+c/tand(a),c,2,2*tand(a),1,lineSpec);
        quiver(e+c/tand(a),c,-e,0,1,lineSpec);
        quiver(c/tand(a),c,-c/tand(a),-c,1,lineSpec);
        
    otherwise
        disp("Wrong ID")
        
    end
end

function [N,parameters] = getParameters(ID)

    ID9 = mod(ID,10);
    ID8 = mod(floor(ID/10),10);
    ID7 = mod(floor(ID/100),10);

    N = mod(ID9,5)+1;
    if(N == 4)
        a = 2 + mod(ID8,5)*0.5;
    else
        a = 15 + mod(ID8,5)*15;
    end

    b = 6 + mod(ID7*ID8,5)*0.5;
    c = 2 + mod(ID7*ID8,5)*0.5;
    e = 2 + mod(ID9+ID8,5)*0.5;
    d = (8 - e)+mod(ID9+ID8,5)*0.5;
    d = d + (10-(e+d));
    f = ID9;

    parameters = [a b c d e f];
end

function quiverNoArrow(a,b,c,d,width,lineSpec)
    q = quiver(a,b,c,d,width,lineSpec);
    q.ShowArrowHead = 'off';
end



