function quiverNoArrow(a,b,c,d,width,lineSpec)
    q = quiver(a,b,c,d,width,lineSpec);
    q.ShowArrowHead = 'off';
end