function [] = nonlinearityDescriptor(ID)    
    
    if or(ID>999999, ID<100000)
        disp("Wrong ID")
        return
    end

    [N,parameters] = getParameters(ID);

    a = parameters(1);
    b = parameters(2);
    c = parameters(3);
    d = parameters(4);
    e = parameters(5);
    
    s = '';
    switch N
        
        case 1
            s = 'f(x) = \n';
            k = tand(a);
            q = -c-k*(-e);
            s = [s num2str(tand(a)) 'x + (' num2str(q) ')  if (x < ' num2str(-e) ') \n'];
            s = [s num2str(tand(a)) 'x + (' num2str(q) ') if (x <= 0' ' and x↑) \n'];
            s = [s '0 if (x <= 0' ' and x↓) \n'];
            s = [s '0 if (x <= ' num2str(d) ') \n'];
            s = [s num2str(b-c) ' if (x > ' num2str(d) ')'];
 
        case 2    
            s = 'f(x) = \n';
            s = [s num2str(-c) ' if (x < ' num2str(-(d-e)) ')\n'];
            s = [s '0 if (x <= ' num2str(e) ')\n'];
            s = [s '0 if (x <= ' num2str(2*e) ' and x↑)\n'];
            k = tand(a);
            q = -k*(2*e);
            s = [s num2str(k) 'x  + (' num2str(q) ') if (x <= ' num2str(2*e+(b-c)/tand(a)) ' and x↑)\n'];
            s = [s num2str(b-c) ' if (x ∈ (' num2str(e) ';' num2str(e+(b-c)/tand(a)) '> and x↓)\n'];
            k = tand(a);
            q = -k*(e); 
            s = [s num2str(k) 'x + (' num2str(q) ') if (x <= ' num2str(e+(b-c)/tand(a)) ' and x↓)\n'];
            s = [s num2str(b-c) ' if (x >'  num2str(2*e+(b-c)/tand(a)) ')\n']; 
            
        case 3
            s = 'f(x) = \n';
            s = [s num2str(c-b) ' if (x <= ' num2str(-e) ')\n'];
            s = [s num2str(c-b) ' if (x < ' num2str(d-e) ' and x↑)\n' ];
            k = tand(a);
            q = (c-b)-k*((d-e));
            s = [s num2str(k) 'x  + (' num2str(q) ') if (x <= ' num2str(d-e+(b)/tand(a)) ' and x↑)\n'];
            s = [s num2str(c) ' if (x ∈ (' num2str(0) ';' num2str((d-e)+b/tand(a)) '> and x↓) \n'];
            s = [s num2str(0) ' if (x ∈ (' num2str(-e) ';' num2str(0) '> and x↓) \n'];
            s = [s num2str(c) ' if (x > ' num2str((d-e)+b/tand(a)),') \n'];
            
        case 4
            s = 'f(x) = \n';
            s = [s num2str(-c) ' if (x <= ' num2str(-e) ')\n'];
            s = [s num2str(-c) ' if (x <= ' num2str(-a/2) ' and x↑)\n'];
            s = [s num2str(0) ' if (x <= ' num2str(-a/2) ' and x↓)\n'];
            s = [s num2str(0) ' if (x <= ' num2str(a/2) ')\n'];
            s = [s num2str(0) ' if (x <= ' num2str(d) ' and x↑)\n'];
            s = [s num2str(b-c) ' if (x <= ' num2str(d) ' and x↓)\n'];
            s = [s num2str(b-c) ' if (x > ' num2str(d) ')\n'];
            
        case 5
            s = 'f(x) = \n';
            k = (b-c)/2;
            q = -k*(-d+e);
            s = [s num2str(k) 'x  + (' num2str(q) ') if (x <= ' num2str(-d+e) ')\n'];
            s = [s num2str(0) ' if (x <= ' num2str(0) ') \n'];
            s = [s num2str(0) ' if (x <= ' num2str(e) ' and x↑) \n'];
            k = tand(a);
            q = -k*(e);
            s = [s num2str(k) 'x  + (' num2str(q) ') if (x ∈ (' num2str(e) ';' num2str(e+c/k) '> and x↑)\n'];
            s = [s num2str(c) ' if x ∈ ('  num2str(c/k) ';' num2str(e+c/k) '> and x↓)\n'];
            k = tand(a);
            q = -k*(0);
            s = [s num2str(k) 'x  + (' num2str(q) ') if (x ∈ (' num2str(0) ';' num2str(c/k) '> and x↓)\n'];
            k = tand(a);
            q = -k*(e);
            s = [s num2str(k) 'x  + (' num2str(q) ') if (x > ' num2str(e+c/k) ')\n'];
            
        otherwise
           disp("Incorrect N") 
        
    end
      
    
    fprintf([s '\n'])
end